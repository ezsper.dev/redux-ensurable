/**
 * This script builds to distribuition
 */
import * as appRootDir from 'app-root-dir';
import { resolve as pathResolve } from 'path';
import * as fs from 'fs-extra';
import { exec } from '../internal/utils/exec';

// First clear the build output dir.
fs.removeSync(pathResolve(appRootDir.get(), './dist'));

// Ignore all that is not used on client
const ignore = [
  "node_modules",
  "bin",
  "build",
  "dist",
  "test",
  "types.d",
  "sample"
].join(',');

// Then compile to pure JS
exec(`tsc --emitDeclarationOnly --skipLibCheck && babel --source-maps --ignore ${ignore} -d dist --extensions \".ts,.tsx\" .`);

// Copy documents
fs.copySync(
  pathResolve(appRootDir.get(), 'README.md'),
  pathResolve(appRootDir.get(), 'dist/README.md'),
);
// Clear our package json for release
const packageJson = require('../package.json');
packageJson.main = './index.js';
delete packageJson.scripts;
delete packageJson.private;
delete packageJson['lint-staged'];
delete packageJson.jest;
delete packageJson.devDependencies;

fs.writeFileSync(
  pathResolve(appRootDir.get(), './dist/package.json'),
  JSON.stringify(packageJson, null, 2),
);

fs.removeSync(pathResolve(appRootDir.get(), './dist/internal'));

fs.copySync(
  pathResolve(appRootDir.get(), 'LICENSE'),
  pathResolve(appRootDir.get(), 'dist/LICENSE'),
);
