/// <reference types="jest" />
import { createEnsurableStore, action, PossibleAction } from '..';

interface CounterState {
  value: number;
}

enum ActionType {
  SET_VALUE = 'counter.SET_VALUE',
  INCREMENT_VALUE = 'counter.INCREMENT_VALUE',
  DECREMENT_VALUE = 'counter.DECREMENT_VALUE',
}

describe('User methods', () => {
  const createCounterScope = () => {
    const actions = {
      setValue: (value: number) =>
        action({
          type: ActionType.SET_VALUE,
          payload: { value },
        }),
      incrementValue: (value = 1) =>
        action({
          type: ActionType.INCREMENT_VALUE,
          payload: { value },
        }),
      decrementValue: (value = 1) =>
        action({
          type: ActionType.DECREMENT_VALUE,
          payload: { value },
        }),
      setAsyncValue: (value: number) => {
        return (dispatch: any) => {
          dispatch(actions.setValue(value));
        };
      },
    };

    function reducer(state: CounterState = { value: 0 }, action: PossibleAction<typeof actions>) {
      switch (action.type) {
        case ActionType.SET_VALUE:
          return {
            ...state,
            value: action.payload.value,
          };
        case ActionType.INCREMENT_VALUE:
          return {
            ...state,
            value: state.value + action.payload.value,
          };
        case ActionType.DECREMENT_VALUE:
          return {
            ...state,
            value: state.value - action.payload.value,
          };
        default:
          return state;
      }
    }

    return { actions, reducer };
  };

  const store = createEnsurableStore(
    {
      counter: createCounterScope(),
      asyncCounter: {
        async: () => Promise.resolve(createCounterScope()),
      },
    },
  );

  it('sync', () => {
    store.dispatch(store.actions.counter.setValue(1));
    const state = store.getState();
    expect(state.counter.value).toBe(1);
    expect(typeof state.asyncCounter).toBe('undefined');
  });

  it('async', async () => {
    const ensuredStore = await store.ensure(['asyncCounter']);
    ensuredStore.dispatch(store.actions.counter.incrementValue());
    const state = ensuredStore.getState();
    expect(state.counter.value).toBe(2);
    expect(state.asyncCounter.value).toBe(1);
  });
});
