import {
  createStore,
  Reducer,
  Action,
  AnyAction,
  DeepPartial,
  Store,
  StoreEnhancer,
} from 'redux';

/**
 * Utility type that make specified fields required and the rest optional
 */
export type RequiredKeys<T, R extends keyof T> = (
  { map: { [K in R]-?: T[K] } & { [K in Exclude<keyof T, R>]?: T[K] } }
  ) extends { map: infer U }
  ? { [K in keyof U]: U[K] }
  : never;

/**
 * Utility type that matches values of an object and return the matching keys
 */
export type MatchValues<T, V> = Extract<{ [K in keyof T]: { key: K, type: T[K] } }[keyof T], { key: string, type: V }> extends { key: infer K }
  ? (K extends keyof T ? K : never)
  : never;


/**
 * Utility type that optionalize object keys that have optional values
 */
export type Optionalize<T> = RequiredKeys<T, MatchValues<{
  [K in keyof T]: T[K] extends never
    ? false
    : (
      Extract<T[K], undefined> extends never
        ? (
          T[K] extends {}
            ? (
              keyof T[K] extends never
                ? false
                : (
                  Partial<T[K]> extends T[K]
                    ? false
                    : true
                  )
              )
            : true
          )
        : false
      )
}, true>>;

export type EnsurableStoreFixedScopeReducer<R extends Reducer<any, Action> = Reducer<any, AnyAction>> =
  R | { reducer: R };

export type EnsurableStoreAsyncScopeReducer<R extends Reducer<any, Action> = Reducer<any, AnyAction>> =
  { async: () => Promise<R | { reducer: R }> };

export type EnsurableStoreAsyncModuleScopeReducer<R extends Reducer<any, Action> = Reducer<any, AnyAction>> =
  { async: () => Promise<{ default: R | { reducer: R } } | { reducer: R }> };

export type EnsurableStoreScopeReducer<S = any, A extends Action = AnyAction> =
  EnsurableStoreAsyncModuleScopeReducer<Reducer<S, A>>
  | EnsurableStoreAsyncScopeReducer<Reducer<S, A>>
  | EnsurableStoreFixedScopeReducer<Reducer<S, A>>;

export type EnsurableStoreActionMapObject<A = (...args: any[]) => any> = {
  [actionKey: string]: A;
};

export type EnsurableStoreFixedScopeAction<M extends EnsurableStoreActionMapObject> =
  { actions: M };

export type EnsurableStoreAsyncScopeAction<M extends EnsurableStoreActionMapObject> =
  { async: () => Promise<{ actions: M }> };

export type EnsurableStoreAsyncModuleScopeAction<M extends EnsurableStoreActionMapObject> =
  { async: () => Promise<{ default: { actions: M } } | { actions: M }> };

export type EnsurableStoreScopeAction<M extends EnsurableStoreActionMapObject = EnsurableStoreActionMapObject> =
  EnsurableStoreAsyncModuleScopeAction<M>
  | EnsurableStoreAsyncScopeAction<M>
  | EnsurableStoreFixedScopeAction<M>;

export type EnsurableStoreScopeMapObject<A extends Action = AnyAction>= {
  [scopeKey: string]: EnsurableStoreScopeReducer<any, A>;
}

export type EnsurableStoreScopeState<AR, E extends true | false = false> =
  AR extends EnsurableStoreAsyncModuleScopeReducer<infer R>
    ? (true extends E ? ReturnType<R> : (ReturnType<R> | undefined))
    : (
      AR extends EnsurableStoreAsyncScopeReducer<infer R>
        ? (true extends E ? ReturnType<R> : (ReturnType<R> | undefined))
        : (
          AR extends EnsurableStoreFixedScopeReducer<infer R>
            ? ReturnType<R>
            : never
          )
      );

export type EnsurableStoreScopeActionAssign<AR, E extends true | false = false> =
  AR extends EnsurableStoreAsyncModuleScopeAction<infer A>
    ? (true extends E ? A : (A | undefined))
    : (
      AR extends EnsurableStoreAsyncScopeAction<infer A>
        ? (true extends E ? A : (A | undefined))
        : (
          AR extends EnsurableStoreFixedScopeAction<infer A>
            ? A
            : never
          )
      );

export type EnsurableStoreState<M extends EnsurableStoreScopeMapObject, E extends MatchEnsureAsync<M> = never, S = any> =
  Optionalize<{ [K in keyof M]: EnsurableStoreScopeState<M[K], K extends MatchEnsureAsync<M> ? (K extends E ? true : false) : true> }>;

export type EnsurableStoreAction<M extends EnsurableStoreScopeMapObject, E extends MatchEnsureAsync<M> = never, S = any> =
  Optionalize<{ [K in MatchValues<M, EnsurableStoreScopeAction>]: EnsurableStoreScopeActionAssign<M[K], K extends MatchEnsureAsync<M> ? (K extends E ? true : false) : true> }>;

export type MatchEnsureAsync<M extends EnsurableStoreScopeMapObject<any>> = MatchValues<M, EnsurableStoreAsyncModuleScopeReducer | EnsurableStoreAsyncScopeReducer>;

export interface StoreEnsure<M extends EnsurableStoreScopeMapObject<any>, E extends MatchEnsureAsync<M> = never, S = any, A extends Action = AnyAction> extends Store<EnsurableStoreState<M, E, S>, A> {
  readonly scopes: M;
  readonly loadState: {
    [K in MatchEnsureAsync<M>]: K extends E ? true : false;
  };
  readonly actions: EnsurableStoreAction<M, E>;
  load<K extends MatchEnsureAsync<M>>(scopes: K[]): void;
  ensure<EE extends MatchEnsureAsync<M>>(ensure: EE[]): Promise<StoreEnsure<M, E | EE, S, A>>;
  ensureAll(): Promise<StoreEnsure<M, MatchEnsureAsync<M>, S, A>>;
}

export interface EnsurableStoreCreator {
  <A extends Action, Ext, StateExt, M extends EnsurableStoreScopeMapObject<A>>(
    scopes: M,
    enhancer?: StoreEnhancer<Ext, StateExt>
  ): StoreEnsure<M, never, StateExt, A> & Ext
  <S, A extends Action, Ext, StateExt, M extends EnsurableStoreScopeMapObject<A>>(
    scopes: M,
    preloadedState?: DeepPartial<EnsurableStoreState<M, never>>,
    enhancer?: StoreEnhancer<Ext>
  ): StoreEnsure<M, never, StateExt, A> & Ext;
}

export type PossibleAction<M extends EnsurableStoreScopeMapObject> =
  (
    { __: { [K in MatchValues<M, (...args: any[]) => Action>]: M[K] extends (...args: any[]) => Action ? ReturnType<M[K]> : never }} extends { __: infer U }
      ? U[keyof U]
      : never
  );

export enum ReduxEnsurableType {
  LOAD = 'redux-ensurable:LOAD',
  LOADED = 'redux-ensurable:LOADED',
  LOAD_FAIL = 'redux-ensurable:LOAD_FAIL',
}

export const createEnsurableStore: EnsurableStoreCreator = <any>((
  scopes: EnsurableStoreScopeMapObject<any>,
  ...args: any[]
) => {
  const loadState: any = {};
  const actions: any = {};
  const loadScopes: any = {};
  const loadedReducers: any = {};
  for (const scopeKey of Object.keys(scopes)) {
    const scope = scopes[scopeKey];
    const includeScope = (scope: any) => {
      if (typeof scope === 'function') {
        loadedReducers[scopeKey] = scope;
      } else {
        if ('actions' in scope) {
          actions[scopeKey] = (<any>scope).actions;
        }
        loadedReducers[scopeKey] = scope.reducer;
      }
    };
    if (typeof scope === 'function') {
      includeScope(scope);
    } else if (typeof scope === 'object' && scope != null) {
      if (typeof (<any>scope).async === 'function') {
        loadScopes[scopeKey] = () => {
          store.dispatch({
            type: ReduxEnsurableType.LOAD,
            payload: { scopeKey },
          });
          return (<any>scope).async()
            .then((scope: any) => {
              includeScope(scope);
              loadState[scopeKey] = true;
              store.dispatch({
                type: ReduxEnsurableType.LOADED,
                payload: { scopeKey },
              });
              return scope;
            })
            .catch((error: Error) => {
              store.dispatch({
                type: ReduxEnsurableType.LOAD_FAIL,
                payload: { scopeKey, error } },
              );
            });
        };
        loadState[scopeKey] = false;
      } else {
        includeScope(scope);
      }
    }
  }
  Object.seal(loadState);
  const reducer = (state: any = {}, action: any) => {
    for (const scopeKey of Object.keys(loadedReducers)) {
      state[scopeKey] = loadedReducers[scopeKey](state[scopeKey] == null ? undefined : state[scopeKey], action);
    }
    return state;
  };
  const store = <any>createStore(
    reducer,
    ...args,
  );
  store.actions = actions;
  store.loadState = loadState;
  store.scopes = scopes;
  store.load = async (scopeKeys: string[]) => {
    for (const scopeKey of scopeKeys) {
      Promise.resolve(loadScopes[scopeKey]())
        .catch(error => {
          console.error(error);
        });
    }
  };
  store.ensure = async (scopeKeys: string[]) => {
    const promises: any[] = [];
    for (const scopeKey of scopeKeys) {
      promises.push(loadScopes[scopeKey]());
    }
    await Promise.all(promises);
    return store;
  };
  store.ensureAll = () => store.ensure(Object.keys(loadState));
  return store;
});

export function action<T extends string, A extends Action<T>>(action: A): A {
  return action;
}