"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

require("regenerator-runtime/runtime");

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

require("core-js/modules/es6.promise");

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _ = require("..");

/// <reference types="jest" />
var ActionType;

(function (ActionType) {
  ActionType["SET_VALUE"] = "counter.SET_VALUE";
  ActionType["INCREMENT_VALUE"] = "counter.INCREMENT_VALUE";
  ActionType["DECREMENT_VALUE"] = "counter.DECREMENT_VALUE";
})(ActionType || (ActionType = {}));

describe('User methods', function () {
  var createCounterScope = function createCounterScope() {
    var actions = {
      setValue: function setValue(value) {
        return (0, _.action)({
          type: ActionType.SET_VALUE,
          payload: {
            value: value
          }
        });
      },
      incrementValue: function incrementValue() {
        var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
        return (0, _.action)({
          type: ActionType.INCREMENT_VALUE,
          payload: {
            value: value
          }
        });
      },
      decrementValue: function decrementValue() {
        var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
        return (0, _.action)({
          type: ActionType.DECREMENT_VALUE,
          payload: {
            value: value
          }
        });
      },
      setAsyncValue: function setAsyncValue(value) {
        return function (dispatch) {
          dispatch(actions.setValue(value));
        };
      }
    };

    function reducer() {
      var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
        value: 0
      };
      var action = arguments.length > 1 ? arguments[1] : undefined;

      switch (action.type) {
        case ActionType.SET_VALUE:
          return (0, _objectSpread2.default)({}, state, {
            value: action.payload.value
          });

        case ActionType.INCREMENT_VALUE:
          return (0, _objectSpread2.default)({}, state, {
            value: state.value + action.payload.value
          });

        case ActionType.DECREMENT_VALUE:
          return (0, _objectSpread2.default)({}, state, {
            value: state.value - action.payload.value
          });

        default:
          return state;
      }
    }

    return {
      actions: actions,
      reducer: reducer
    };
  };

  var store = (0, _.createEnsurableStore)({
    counter: createCounterScope(),
    asyncCounter: {
      async: function async() {
        return Promise.resolve(createCounterScope());
      }
    }
  });
  it('sync', function () {
    store.dispatch(store.actions.counter.setValue(1));
    var state = store.getState();
    expect(state.counter.value).toBe(1);
    expect((0, _typeof2.default)(state.asyncCounter)).toBe('undefined');
  });
  it('async',
  /*#__PURE__*/
  (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    var ensuredStore, state;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return store.ensure(['asyncCounter']);

          case 2:
            ensuredStore = _context.sent;
            ensuredStore.dispatch(store.actions.counter.incrementValue());
            state = ensuredStore.getState();
            expect(state.counter.value).toBe(2);
            expect(state.asyncCounter.value).toBe(1);

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  })));
});
//# sourceMappingURL=simple.test.js.map