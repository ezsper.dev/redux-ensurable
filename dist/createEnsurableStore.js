"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.action = action;
exports.createEnsurableStore = exports.ReduxEnsurableType = void 0;

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

require("core-js/modules/es6.string.iterator");

require("core-js/modules/es6.promise");

require("core-js/modules/es7.symbol.async-iterator");

require("core-js/modules/es6.symbol");

require("regenerator-runtime/runtime");

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

require("core-js/modules/web.dom.iterable");

require("core-js/modules/es6.array.iterator");

require("core-js/modules/es6.object.keys");

require("core-js/modules/es6.object.seal");

var _redux = require("redux");

var ReduxEnsurableType;
exports.ReduxEnsurableType = ReduxEnsurableType;

(function (ReduxEnsurableType) {
  ReduxEnsurableType["LOAD"] = "redux-ensurable:LOAD";
  ReduxEnsurableType["LOADED"] = "redux-ensurable:LOADED";
  ReduxEnsurableType["LOAD_FAIL"] = "redux-ensurable:LOAD_FAIL";
})(ReduxEnsurableType || (exports.ReduxEnsurableType = ReduxEnsurableType = {}));

var createEnsurableStore = function createEnsurableStore(scopes) {
  var loadState = {};
  var actions = {};
  var loadScopes = {};
  var loadedReducers = {};

  var _arr = Object.keys(scopes);

  var _loop = function _loop() {
    var scopeKey = _arr[_i];
    var scope = scopes[scopeKey];

    var includeScope = function includeScope(scope) {
      if (typeof scope === 'function') {
        loadedReducers[scopeKey] = scope;
      } else {
        if ('actions' in scope) {
          actions[scopeKey] = scope.actions;
        }

        loadedReducers[scopeKey] = scope.reducer;
      }
    };

    if (typeof scope === 'function') {
      includeScope(scope);
    } else if ((0, _typeof2.default)(scope) === 'object' && scope != null) {
      if (typeof scope.async === 'function') {
        loadScopes[scopeKey] = function () {
          store.dispatch({
            type: ReduxEnsurableType.LOAD,
            payload: {
              scopeKey: scopeKey
            }
          });
          return scope.async().then(function (scope) {
            includeScope(scope);
            loadState[scopeKey] = true;
            store.dispatch({
              type: ReduxEnsurableType.LOADED,
              payload: {
                scopeKey: scopeKey
              }
            });
            return scope;
          }).catch(function (error) {
            store.dispatch({
              type: ReduxEnsurableType.LOAD_FAIL,
              payload: {
                scopeKey: scopeKey,
                error: error
              }
            });
          });
        };

        loadState[scopeKey] = false;
      } else {
        includeScope(scope);
      }
    }
  };

  for (var _i = 0; _i < _arr.length; _i++) {
    _loop();
  }

  Object.seal(loadState);

  var reducer = function reducer() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var action = arguments.length > 1 ? arguments[1] : undefined;

    var _arr2 = Object.keys(loadedReducers);

    for (var _i2 = 0; _i2 < _arr2.length; _i2++) {
      var _scopeKey = _arr2[_i2];
      state[_scopeKey] = loadedReducers[_scopeKey](state[_scopeKey] == null ? undefined : state[_scopeKey], action);
    }

    return state;
  };

  for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    args[_key - 1] = arguments[_key];
  }

  var store = _redux.createStore.apply(void 0, [reducer].concat(args));

  store.actions = actions;
  store.loadState = loadState;
  store.scopes = scopes;

  store.load =
  /*#__PURE__*/
  function () {
    var _ref = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(scopeKeys) {
      var _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, _scopeKey2;

      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _iteratorNormalCompletion = true;
              _didIteratorError = false;
              _iteratorError = undefined;
              _context.prev = 3;

              for (_iterator = scopeKeys[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                _scopeKey2 = _step.value;
                Promise.resolve(loadScopes[_scopeKey2]()).catch(function (error) {
                  console.error(error);
                });
              }

              _context.next = 11;
              break;

            case 7:
              _context.prev = 7;
              _context.t0 = _context["catch"](3);
              _didIteratorError = true;
              _iteratorError = _context.t0;

            case 11:
              _context.prev = 11;
              _context.prev = 12;

              if (!_iteratorNormalCompletion && _iterator.return != null) {
                _iterator.return();
              }

            case 14:
              _context.prev = 14;

              if (!_didIteratorError) {
                _context.next = 17;
                break;
              }

              throw _iteratorError;

            case 17:
              return _context.finish(14);

            case 18:
              return _context.finish(11);

            case 19:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this, [[3, 7, 11, 19], [12,, 14, 18]]);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }();

  store.ensure =
  /*#__PURE__*/
  function () {
    var _ref2 = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2(scopeKeys) {
      var promises, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, _scopeKey3;

      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              promises = [];
              _iteratorNormalCompletion2 = true;
              _didIteratorError2 = false;
              _iteratorError2 = undefined;
              _context2.prev = 4;

              for (_iterator2 = scopeKeys[Symbol.iterator](); !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                _scopeKey3 = _step2.value;
                promises.push(loadScopes[_scopeKey3]());
              }

              _context2.next = 12;
              break;

            case 8:
              _context2.prev = 8;
              _context2.t0 = _context2["catch"](4);
              _didIteratorError2 = true;
              _iteratorError2 = _context2.t0;

            case 12:
              _context2.prev = 12;
              _context2.prev = 13;

              if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                _iterator2.return();
              }

            case 15:
              _context2.prev = 15;

              if (!_didIteratorError2) {
                _context2.next = 18;
                break;
              }

              throw _iteratorError2;

            case 18:
              return _context2.finish(15);

            case 19:
              return _context2.finish(12);

            case 20:
              _context2.next = 22;
              return Promise.all(promises);

            case 22:
              return _context2.abrupt("return", store);

            case 23:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, this, [[4, 8, 12, 20], [13,, 15, 19]]);
    }));

    return function (_x2) {
      return _ref2.apply(this, arguments);
    };
  }();

  store.ensureAll = function () {
    return store.ensure(Object.keys(loadState));
  };

  return store;
};

exports.createEnsurableStore = createEnsurableStore;

function action(action) {
  return action;
}
//# sourceMappingURL=createEnsurableStore.js.map