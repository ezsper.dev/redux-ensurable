import { Reducer, Action, AnyAction, DeepPartial, Store, StoreEnhancer } from 'redux';
/**
 * Utility type that make specified fields required and the rest optional
 */
export declare type RequiredKeys<T, R extends keyof T> = ({
    map: {
        [K in R]-?: T[K];
    } & {
        [K in Exclude<keyof T, R>]?: T[K];
    };
}) extends {
    map: infer U;
} ? {
    [K in keyof U]: U[K];
} : never;
/**
 * Utility type that matches values of an object and return the matching keys
 */
export declare type MatchValues<T, V> = Extract<{
    [K in keyof T]: {
        key: K;
        type: T[K];
    };
}[keyof T], {
    key: string;
    type: V;
}> extends {
    key: infer K;
} ? (K extends keyof T ? K : never) : never;
/**
 * Utility type that optionalize object keys that have optional values
 */
export declare type Optionalize<T> = RequiredKeys<T, MatchValues<{
    [K in keyof T]: T[K] extends never ? false : (Extract<T[K], undefined> extends never ? (T[K] extends {} ? (keyof T[K] extends never ? false : (Partial<T[K]> extends T[K] ? false : true)) : true) : false);
}, true>>;
export declare type EnsurableStoreFixedScopeReducer<R extends Reducer<any, Action> = Reducer<any, AnyAction>> = R | {
    reducer: R;
};
export declare type EnsurableStoreAsyncScopeReducer<R extends Reducer<any, Action> = Reducer<any, AnyAction>> = {
    async: () => Promise<R | {
        reducer: R;
    }>;
};
export declare type EnsurableStoreAsyncModuleScopeReducer<R extends Reducer<any, Action> = Reducer<any, AnyAction>> = {
    async: () => Promise<{
        default: R | {
            reducer: R;
        };
    } | {
        reducer: R;
    }>;
};
export declare type EnsurableStoreScopeReducer<S = any, A extends Action = AnyAction> = EnsurableStoreAsyncModuleScopeReducer<Reducer<S, A>> | EnsurableStoreAsyncScopeReducer<Reducer<S, A>> | EnsurableStoreFixedScopeReducer<Reducer<S, A>>;
export declare type EnsurableStoreActionMapObject<A = (...args: any[]) => any> = {
    [actionKey: string]: A;
};
export declare type EnsurableStoreFixedScopeAction<M extends EnsurableStoreActionMapObject> = {
    actions: M;
};
export declare type EnsurableStoreAsyncScopeAction<M extends EnsurableStoreActionMapObject> = {
    async: () => Promise<{
        actions: M;
    }>;
};
export declare type EnsurableStoreAsyncModuleScopeAction<M extends EnsurableStoreActionMapObject> = {
    async: () => Promise<{
        default: {
            actions: M;
        };
    } | {
        actions: M;
    }>;
};
export declare type EnsurableStoreScopeAction<M extends EnsurableStoreActionMapObject = EnsurableStoreActionMapObject> = EnsurableStoreAsyncModuleScopeAction<M> | EnsurableStoreAsyncScopeAction<M> | EnsurableStoreFixedScopeAction<M>;
export declare type EnsurableStoreScopeMapObject<A extends Action = AnyAction> = {
    [scopeKey: string]: EnsurableStoreScopeReducer<any, A>;
};
export declare type EnsurableStoreScopeState<AR, E extends true | false = false> = AR extends EnsurableStoreAsyncModuleScopeReducer<infer R> ? (true extends E ? ReturnType<R> : (ReturnType<R> | undefined)) : (AR extends EnsurableStoreAsyncScopeReducer<infer R> ? (true extends E ? ReturnType<R> : (ReturnType<R> | undefined)) : (AR extends EnsurableStoreFixedScopeReducer<infer R> ? ReturnType<R> : never));
export declare type EnsurableStoreScopeActionAssign<AR, E extends true | false = false> = AR extends EnsurableStoreAsyncModuleScopeAction<infer A> ? (true extends E ? A : (A | undefined)) : (AR extends EnsurableStoreAsyncScopeAction<infer A> ? (true extends E ? A : (A | undefined)) : (AR extends EnsurableStoreFixedScopeAction<infer A> ? A : never));
export declare type EnsurableStoreState<M extends EnsurableStoreScopeMapObject, E extends MatchEnsureAsync<M> = never, S = any> = Optionalize<{
    [K in keyof M]: EnsurableStoreScopeState<M[K], K extends MatchEnsureAsync<M> ? (K extends E ? true : false) : true>;
}>;
export declare type EnsurableStoreAction<M extends EnsurableStoreScopeMapObject, E extends MatchEnsureAsync<M> = never, S = any> = Optionalize<{
    [K in MatchValues<M, EnsurableStoreScopeAction>]: EnsurableStoreScopeActionAssign<M[K], K extends MatchEnsureAsync<M> ? (K extends E ? true : false) : true>;
}>;
export declare type MatchEnsureAsync<M extends EnsurableStoreScopeMapObject<any>> = MatchValues<M, EnsurableStoreAsyncModuleScopeReducer | EnsurableStoreAsyncScopeReducer>;
export interface StoreEnsure<M extends EnsurableStoreScopeMapObject<any>, E extends MatchEnsureAsync<M> = never, S = any, A extends Action = AnyAction> extends Store<EnsurableStoreState<M, E, S>, A> {
    readonly scopes: M;
    readonly loadState: {
        [K in MatchEnsureAsync<M>]: K extends E ? true : false;
    };
    readonly actions: EnsurableStoreAction<M, E>;
    load<K extends MatchEnsureAsync<M>>(scopes: K[]): void;
    ensure<EE extends MatchEnsureAsync<M>>(ensure: EE[]): Promise<StoreEnsure<M, E | EE, S, A>>;
    ensureAll(): Promise<StoreEnsure<M, MatchEnsureAsync<M>, S, A>>;
}
export interface EnsurableStoreCreator {
    <A extends Action, Ext, StateExt, M extends EnsurableStoreScopeMapObject<A>>(scopes: M, enhancer?: StoreEnhancer<Ext, StateExt>): StoreEnsure<M, never, StateExt, A> & Ext;
    <S, A extends Action, Ext, StateExt, M extends EnsurableStoreScopeMapObject<A>>(scopes: M, preloadedState?: DeepPartial<EnsurableStoreState<M, never>>, enhancer?: StoreEnhancer<Ext>): StoreEnsure<M, never, StateExt, A> & Ext;
}
export declare type PossibleAction<M extends EnsurableStoreScopeMapObject> = ({
    __: {
        [K in MatchValues<M, (...args: any[]) => Action>]: M[K] extends (...args: any[]) => Action ? ReturnType<M[K]> : never;
    };
} extends {
    __: infer U;
} ? U[keyof U] : never);
export declare enum ReduxEnsurableType {
    LOAD = "redux-ensurable:LOAD",
    LOADED = "redux-ensurable:LOADED",
    LOAD_FAIL = "redux-ensurable:LOAD_FAIL"
}
export declare const createEnsurableStore: EnsurableStoreCreator;
export declare function action<T extends string, A extends Action<T>>(action: A): A;
